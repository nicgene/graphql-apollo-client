import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import { apolloProvider } from './vue-apollo';

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
  router,
  apolloProvider,
  render: (h) => h(App),
}).$mount('#app');
