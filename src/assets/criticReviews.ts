export default [
  {
    id: '09fe42a6-aab5-11e9-a2a3-2a2ae2dbcce4',
    score: 7,
    author: {
      name: 'Variety',
    },
    content: `Dodger Dogs were doing a robust business Saturday night during Paul McCartney’s appearance at the stadium
      that shares the delicacy’s name. He is not one of those performers who tries imposing dietary restrictions on
      the venues he plays, if that were even possible on the rarefied stadium tour circuit. Nor did food or any kind
      of health regimen arise as a subject as he bantered with the crowd.`,
    uri: 'https://www.test.com',
    created: '2019-07-14 10:01:00',
    updated: '',
  },
  {
    id: '09fe45e4-aab5-11e9-a2a3-2a2ae2dbcce4',
    score: 8,
    author: {
      name: 'Billboard',
    },
    content: `In an epic, three-hour concert at Los Angeles' Dodger Stadium on Saturday night that spanned a whopping 38
      songs, Paul McCartney took a rare quiet moment to urge the 50,000-plus fans staring up at him to tell people they
      love them early and often. "If you've got something really nice that you want to say to someone, you sometimes put
      it off," McCartney said onstage. "You say, 'Oh, I'll tell them tomorrow, or I'll catch them next week.' And
      sometimes it can be too late and you don't get to say it."`,
    uri: 'https://www.test.com',
    created: '2019-07-14 14:00:00',
    updated: '',
  },
  {
    id: '09fe4742-aab5-11e9-a2a3-2a2ae2dbcce4',
    score: 7,
    author: {
      name: 'Los Angeles Times',
    },
    content: `What a Beatle wants, a Beatle generally gets, even when it may require a bit of harmless subterfuge, as
      proved to be the case Saturday during Paul McCartney’s sold-out final U.S. stop on his 2019 Freshen Up tour.`,
    uri: 'https://www.test.com',
    created: '2019-07-14 12:52:00',
    updated: '',
  },
  {
    id: '09fe486e-aab5-11e9-a2a3-2a2ae2dbcce4',
    score: 6,
    author: {
      name: 'UPROXX',
    },
    content: `Though I’ve seen him once before — a send-off Jersey stadium show at MetLife in 2016 right before I moved
      to Los Angeles — watching McCartney take the stage at Dodger Stadium for his Freshen Up tour had a certain
      bittersweetness to it, given the knowledge that the Beatles themselves played their second-to-last show on that
      same stage in 1966. Paul, ever the historian, clocked that, of course, noting that there were probably a few
      members in the 2019 crowd who had been there some fifty years ago.`,
    uri: 'https://www.test.com',
    created: '2019-07-15 11:30:00',
    updated: '',
  },
];
