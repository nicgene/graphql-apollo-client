export default [
  {
    id: '09fe3c3e-aab5-11e9-a2a3-2a2ae2dbcce4',
    grade: 'P',
    author: {
      name: 'David Villanueva',
    },
    content: `Thank you #PaulMcCartney for helping me check off an item off of my #bucklist! The concert at #Dodgers
      stadium was amazing! And he brought out Ringo! #thebeatles. Golden Slumbers was a nice way to end the show.`,
    source: 'twitter',
    uri: 'https://twitter.com/authordavidv/status/1150312576107962368',
    created: '2019-07-14 12:54:00',
    updated: '',
  },
  {
    id: '09fe401c-aab5-11e9-a2a3-2a2ae2dbcce4',
    grade: 'M',
    author: {
      name: 'steph',
    },
    content: `BEST CONCERT EVER! A dream come true 🥰😭🙌🏽 Paul even brought out Ringo!!!`,
    source: 'twitter',
    uri: 'https://twitter.com/xoxo_esss/status/1150312643447549952',
    created: '2019-07-14 12:54:00',
    updated: '',
  },
  {
    id: '09fe4170-aab5-11e9-a2a3-2a2ae2dbcce4',
    grade: 'P',
    author: {
      name: 'KimberleyG.',
    },
    content: `Paul McCartney is amazing! What an experience! I brought my teenagers & they were completely blown away.`,
    source: '',
    uri: '',
    created: '2019-07-14 02:06:00',
    updated: '',
  },
];
