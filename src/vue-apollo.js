import { InMemoryCache } from 'apollo-cache-inmemory';
import { persistCache } from 'apollo-cache-persist';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { HttpLink } from 'apollo-link-http';
import { withClientState } from 'apollo-link-state';
import Vue from 'vue';
import VueApollo from 'vue-apollo';
// import UpdateAlert from './graphql/UpdateAlert.gql';
// import UpdateErrors from './graphql/UpdateErrors.gql';
import { WebSocketLink } from 'apollo-link-ws';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';

Vue.use(VueApollo);

const cache = new InMemoryCache();

persistCache({
  cache,
  storage: window.localStorage,
});

const httpLink = new HttpLink({
  uri: process.env.VUE_APP_GRAPHQL_HTTP_URI
});

const wsLink = new WebSocketLink({
  uri: process.env.VUE_APP_GRAPHQL_WS_URI,
  options: {
    reconnect: true,
  },
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
  },
  wsLink,
  httpLink
);

const authLink = setContext((_, { headers }) =>
{
  let token = JSON.parse(localStorage.getItem('auth-token'))
  
  return {
    headers: {
      authorization: token ? `Bearer ${token.accessToken}` : '',
      ...headers
    }
  }
});

const errorLink = onError(({ graphQLErrors, networkError }) =>
{
  let apolloClient = apolloProvider.defaultClient;

  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path, extensions }) => {
      console.error(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
      let input = [];
      if (extensions.code === 'BAD_USER_INPUT') {
        extensions.exception.invalidArgs.map(error => {
          input.push(error.title);
        });
      }

      // apolloClient.mutate({
      //   mutation: UpdateErrors,
      //   variables: {
      //     input
      //   }
      // });
    });
  }
  
  if (networkError) {
    console.log(`[Network error]: ${networkError}`);    
    // apolloClient.mutate({
    //   mutation: UpdateAlert,
    //   variables: {
    //     message: `[Network error]: ${networkError}`,
    //     variant: 'danger'
    //   }
    // });
  }
});

const stateLink = withClientState({
  cache,
  resolvers: {
    Mutation: {
      // updateAlert: (_, { message = '', variant = 'success', show = true }, { cache }) => {
      //   let data = {
      //     alert: {
      //       __typename: 'Alert',
      //       message: message,
      //       variant: variant,
      //       show: show
      //     }
      //   }
      //   cache.writeData({ data });
      //   return null;
      // },
      // updateErrors: (_, { input = [] }, { cache }) => {
      //   let errors = []        
      //   input.forEach(error => {
      //     errors.push({
      //       __typename: 'Error',
      //       message: error
      //     })
      //   })

      //   let data = {
      //     errors
      //   }
      //   cache.writeData({ data });
      //   return null;
      // }
    }
  },
  defaults: {
    // alert: {
    //   __typename: 'Alert',
    //   message: '',
    //   variant: 'success',
    //   show: false
    // }
  }
})

const apolloClient = new ApolloClient({
  link: ApolloLink.from([
    errorLink,
    authLink,
    stateLink,
    // httpLink
    link
  ]),
  cache,
  connectToDevTools: true
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
    $loadingKey: 'loading'
  }
});

const clearStore = async () =>
{
  try {
    await apolloProvider.defaultClient.clearStore()
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log('%cError on cache reset (login)', 'color: orange;', e.message)
  }
}

export { apolloProvider, clearStore };

// export default apolloProvider;
